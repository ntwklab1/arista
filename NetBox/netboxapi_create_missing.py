import pyeapi
from pynetbox import api


def get_switch_hostname(connection_profile):
    try:
        # Connect to the Arista switch using the connection profile
        connection = pyeapi.client.connect(**connection_profile)
        node = pyeapi.client.Node(connection)

        # Execute the show running-config command
        response = node.enable(["show running-config"])

        # Retrieve the hostname from the running configuration

        header = response[0]["result"]["header"][0]  # Get the first element of the 'header' list
        device_info = header.split(':')[1].strip()  # Split the string by ':' and retrieve the second element
        device_name = device_info.split()[0]  # Split the device_info by whitespace and retrieve the first element

        print(device_name)
        return device_name

    except pyeapi.eapilib.CommandError as e:
        print(f"Error: {e}")

    return None




def get_networks_vlans():
    try:
        # Connect to the Arista switch using the connection profile
        connection = pyeapi.client.connect(**connection_profile)
        node = pyeapi.client.Node(connection)

        # Execute the command to get VLAN information
        vlan_output = node.enable("show vlan")
        vlans = vlan_output[0]["result"]["vlans"]

        # Execute the command to get network information
        network_output = node.enable("show ip interface brief")
        # print(network_output)

        networks = []
        for interface, interface_info in network_output[0]['result']['interfaces'].items():
            if 'interfaceAddress' in interface_info:
                ip_address = interface_info['interfaceAddress']['ipAddr']['address']
                mask_len = interface_info['interfaceAddress']['ipAddr']['maskLen']
                formatted_ip = f"{ip_address}/{mask_len}"
                # print(f"{interface}: {formatted_ip}")
                networks.append(formatted_ip)


        # Create a connection to Netbox
        nb = api(url="http://127.0.0.1:8000", token="0123456789abcdef0123456789abcdef01234567")

        # Get all VLANs from Netbox
        netbox_vlans = nb.ipam.vlans.all()

        # Get all networks from Netbox
        netbox_networks = nb.ipam.prefixes.all()


        nb_mising = {'VLANs':[]}
        
        # Print missing VLANs from Netbox
        print("Missing VLANs from Netbox:")
        for vlan_id, vlan_info in vlans.items():
            vlan_exists = any(vlan.vid == int(vlan_id) for vlan in netbox_vlans)
            if not vlan_exists:
                nb_mising['VLANs'].append(vlan_id)


        print()
        netbox_net_set = []
        for net in netbox_networks:
            netbox_net_set.append(net)

        nb_nets = set(netbox_net_set) - set(networks)
        sw_nets = set(networks) - set(netbox_net_set)
        common_nets = set(netbox_net_set) & set(networks)
        nb_mising['networks'] = sw_nets

        print("Networks in Netbox")
        for number in nb_nets:
            print(number)

        print("\nNetworks on the Switch:")
        for number in sw_nets:
            print(number)

        print("\nNetworks in Both:")
        for number in common_nets:
            print(number)

        return nb_mising


    except pyeapi.eapilib.CommandError as e:
        print(f"Error: {e}")


if __name__ == "__main__":

    # Define the connection profile for Arista EAPI
    connection_profile = {
        "transport": "https",
        "host": "172.16.1.11",
        "username": "admin",
        "password": "123abc",
        "port": 443,
    }


    vl_nt_st_list = []
    vl_nt_st_dict = {}

    #Get the networks and vlans from the swithces and netbox
    nb_mising = get_networks_vlans()


    # Get the hostname of the switch
    hostname = get_switch_hostname(connection_profile)
    if "spine" in hostname:
        site = "uk1"
        nb_mising['site'] =  site
    elif "leaf" in hostname:
        site = "uk2"
        nb_mising['site'] =  site

    print(f"\nNB Missing\n{nb_mising}")
    print(nb_mising)


