import pyeapi
from pynetbox import api

class NetboxManager:
    def __init__(self, connection_profile):
        self.connection_profile = connection_profile
        self.nb = api(url="http://127.0.0.1:8000",
                         token="0123456789abcdef0123456789abcdef01234567")

    def get_switch_hostname(self):
        try:
            # Connect to the Arista switch using the connection profile
            connection = pyeapi.client.connect(**self.connection_profile)
            node = pyeapi.client.Node(connection)

            # Execute the show running-config command
            response = node.enable(["show running-config"])

            # Retrieve the hostname from the running configuration
            header = response[0]["result"]["header"][0]
            device_info = header.split(':')[1].strip()
            device_name = device_info.split()[0]

            print(device_name)
            return device_name

        except pyeapi.eapilib.CommandError as e:
            print(f"Error: {e}")

        return None

    def get_networks_vlans(self):
        try:
            # Connect to the Arista switch using the connection profile
            connection = pyeapi.client.connect(**self.connection_profile)
            node = pyeapi.client.Node(connection)

            # Execute the command to get VLAN information
            vlan_output = node.enable("show vlan")
            vlans = vlan_output[0]["result"]["vlans"]

            # Execute the command to get network information
            network_output = node.enable("show ip interface brief")
            networks = []
            for interface, interface_info in network_output[0]['result']['interfaces'].items():
                if 'interfaceAddress' in interface_info:
                    ip_address = interface_info['interfaceAddress']['ipAddr']['address']
                    mask_len = interface_info['interfaceAddress']['ipAddr']['maskLen']
                    formatted_ip = f"{ip_address}/{mask_len}"
                    networks.append(formatted_ip)

            netbox_vlans = self.nb.ipam.vlans.all()
            netbox_networks = self.nb.ipam.prefixes.all()

            nb_mising = {'VLANs': []}

            # Print missing VLANs from Netbox
            print("Missing VLANs from Netbox:")
            for vlan_id, vlan_info in vlans.items():
                vlan_exists = any(vlan.vid == int(vlan_id) for vlan in netbox_vlans)
                if not vlan_exists:
                    nb_mising['VLANs'].append(vlan_id)

            print()
            netbox_net_set = [net for net in netbox_networks]
            nb_nets = set(netbox_net_set) - set(networks)
            sw_nets = set(networks) - set(netbox_net_set)
            common_nets = set(netbox_net_set) & set(networks)
            nb_mising['networks'] = sw_nets

            print("Networks in Netbox")
            for number in nb_nets:
                print(number)

            print("\nNetworks on the Switch:")
            for number in sw_nets:
                print(number)

            print("\nNetworks in Both:")
            for number in common_nets:
                print(number)

            return nb_mising

        except pyeapi.eapilib.CommandError as e:
            print(f"Error: {e}")

    def run(self):
        # Get the networks and VLANs from the switches and Netbox
        nb_missing = self.get_networks_vlans()

        # Get the hostname of the switch
        hostname = self.get_switch_hostname()
        if "spine" in hostname:
            site = "uk1"
            nb_missing['site'] = site
        elif "leaf" in hostname:
            site = "uk2"
            nb_missing['site'] = site

        return nb_missing

if __name__ == "__main__":
    # Define the connection profile for Arista EAPI
    connection_profile = {
        "transport": "https",
        "host": "172.16.1.11",
        "username": "admin",
        "password": "123abc",
        "port": 443,
    }

    netbox_manager = NetboxManager(connection_profile)
    nb_missing = netbox_manager.run()

    print(f"\nNB Missing\n{nb_missing}")
    print(nb_missing)

